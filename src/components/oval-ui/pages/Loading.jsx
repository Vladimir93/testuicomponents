import React from 'react';
import Progress from '../components/Progress';
import Preloader from '../components/Preloader';

import preloader from '../../static/img/preloader.gif';

export default () => (
  <div className="oval-root">
    <Progress width="30%" />
    <Preloader visible animation={preloader} />
  </div>
);
