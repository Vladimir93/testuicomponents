import React from 'react';
import Title from '../components/typography/Title';
import Paragraph from '../components/typography/Paragraph';
import Button from '../components/buttons/Button';
import Logo from '../components/Logo';
import Progress from '../components/Progress';
import SocialButton from '../components/buttons/SocialButton';

import { ReactComponent as Facebook } from '../../static/img/icons/socials/fb.svg';
import { ReactComponent as Telegram } from '../../static/img/icons/socials/telegram.svg';
import { ReactComponent as Twitter } from '../../static/img/icons/socials/twitter.svg';
import { ReactComponent as Vk } from '../../static/img/icons/socials/vk.svg';

import { ReactComponent as HeadsetIcon } from '../../static/img/icons/hardware/headset_mic_24px.svg';
import { ReactComponent as Drafts } from '../../static/img/icons/content/drafts_24px.svg';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title>
        Ваша выплата совершена.
        <br />
        <span className="oval-primary">3000 ₽.</span>
      </Title>
      <Paragraph isGray>
        Будут в течение часа. Подробную информацию мы отправили на почту.
      </Paragraph>
      <SocialButton.Group title="Посоветуйте нас">
        <SocialButton background="#3B5998">
          <Facebook />
        </SocialButton>
        <SocialButton background="#37AEE2">
          <Telegram />
        </SocialButton>
        <SocialButton background="#1DA1F3">
          <Twitter />
        </SocialButton>
        <SocialButton background="#4680C2">
          <Vk />
        </SocialButton>
      </SocialButton.Group>
      <Button textAlign="left" hasLeftIcon bordered>
        <HeadsetIcon />
        <span>Позвонить в службу поддержки</span>
      </Button>
      <Button textAlign="left" hasLeftIcon bordered>
        <Drafts />
        <span>Написать письмо на почту</span>
      </Button>
    </div>
  </div>
);
