import React from 'react';
import Title from '../components/typography/Title';
import Subtitle from '../components/typography/Subtitle';
import Paragraph from '../components/typography/Paragraph';
import Input from '../components/inputs/Input';
import Button from '../components/buttons/Button';
import Logo from '../components/Logo';
import Progress from '../components/Progress';
import Info from '../components/Info';
import Checkbox from '../components/inputs/Checkbox';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title>Получение выплаты</Title>
      <Paragraph isGray classList="oval-mb-56">Пожалуйста, заполните форму</Paragraph>
      <div className="oval-user-info">
        <Subtitle>Мячков Руслан</Subtitle>
        <Paragraph isGray>28.12.1994</Paragraph>
      </div>
      <Info isBlack>
        <strong>№ IP-ZR-000583 </strong>
        <span className="oval-gray">Номер полиса</span>
      </Info>
      <Input.Group>
        <Input placeholder="Серия и номер паспорта" mask="9999 999999" />
        <Input placeholder="Фото паспорта..." type="file" />
      </Input.Group>
      <Input.Group>
        <Input placeholder="Номер банковской карты" mask="9999 9999 9999 9999" />
        <Input placeholder="Месяц" mask="99" width="120px" />
        <Input placeholder="Год" mask="2099" width="120px" />
        <Input placeholder="Фамилия и имя на карте" />
      </Input.Group>
      <Checkbox>
        Подтверждаю факт наступления страхового события и
        <span className="oval-primary"> заявление о наступлении страхового случая</span>
        , согласен(а) на обработку
        <span className="oval-primary"> данных</span>
      </Checkbox>
      <Button>Продолжить</Button>
    </div>
  </div>
);
