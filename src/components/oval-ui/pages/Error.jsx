import React from 'react';
import Title from '../components/typography/Title';
import Paragraph from '../components/typography/Paragraph';
import Logo from '../components/Logo';
import Progress from '../components/Progress';

import error from '../../static/img/error-photo.png';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title>Сбой в работе</Title>
      <Paragraph isGray classList="oval-mb-32">Наша команда уже работает над поломкой, но вы можете:</Paragraph>
      <Paragraph>
        <span className="oval-primary">Позвонить </span>
        в службу поддержки.
        <br />
        Написать нам на
        <span className="oval-primary"> почту </span>
        или в
        <span className="oval-primary"> фейсбуке</span>
      </Paragraph>
    </div>
    <img src={error} alt="" className="oval-error-photo" />
  </div>
);
