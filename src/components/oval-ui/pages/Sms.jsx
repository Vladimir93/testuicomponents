import React from 'react';
import Title from '../components/typography/Title';
import Paragraph from '../components/typography/Paragraph';
import Input from '../components/inputs/Input';
import Button from '../components/buttons/Button';
import Link from '../components/buttons/Link';
import Checkbox from '../components/inputs/Checkbox';
import Logo from '../components/Logo';
import Progress from '../components/Progress';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title>
        Рейс
        <span className="oval-primary"> WM 5002 </span>
        <br />
        задержан
      </Title>
      <Paragraph isGray>
        На 2 часа 16 минут,
        <br />
        сумма выплаты 3000 ₽
      </Paragraph>
      <Input placeholder="Введите код из смс" mask="99999" classList="oval-mb-0" />
      <Link disabled>Отправить код еще раз можно через 27 сек.</Link>
      <Checkbox>
        Подтверждаю факт наступления страхового события и
        <span className="oval-primary"> заявление о наступлении страхового случая, </span>
        согласен(а) на обработку
        <span className="oval-primary"> данных</span>
      </Checkbox>
      <Button>Указать номер карты</Button>
    </div>
  </div>
);
