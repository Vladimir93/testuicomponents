import React from 'react';
import Title from '../components/typography/Title';
import Paragraph from '../components/typography/Paragraph';
import Input from '../components/inputs/Input';
import Button from '../components/buttons/Button';
import Logo from '../components/Logo';
import Progress from '../components/Progress';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title>Ваш рейс задержан?</Title>
      <Paragraph isGray>Пожалуйста, заполните форму для получения страховой выплаты</Paragraph>
      <div className="oval-mb-32">
        <Input placeholder="Номер телефона" mask="+7 (999) 999-99-99" />
        <Input placeholder="Номер рейса" mask="aa 9999" />
      </div>
      <Button>Продолжить</Button>
    </div>
  </div>
);
