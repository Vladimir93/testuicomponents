import React from 'react';
import Title from '../components/typography/Title';
import Subtitle from '../components/typography/Subtitle';
import Paragraph from '../components/typography/Paragraph';
import Input from '../components/inputs/Input';
import Button from '../components/buttons/Button';
import Logo from '../components/Logo';
import Progress from '../components/Progress';
import Info from '../components/Info';
import Checkbox from '../components/inputs/Checkbox';
import People from '../components/People';

import { ReactComponent as Check } from '../../static/img/icons/action/check_circle_24px.svg';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title>
        Рейс
        <span className="oval-primary"> WM 5002 </span>
        <br />
        задержан
      </Title>
      <Paragraph isGray>
        Пожалуйста, укажите, номер телефона каждого застрахованного пассажира
      </Paragraph>
      <People.Group>
        <People name="Мячков Руслан" birthday="28.12.1994">
          <Button selected hasRightIcon width="auto">
            <span>Это я</span>
            <Check />
          </Button>
        </People>
        <People name="Евгений Бирюков" birthday="28.12.1994">
          <Button color="light-gray" bordered hasRightIcon width="auto">
            <span>Это я</span>
            <Check />
          </Button>
          <Input placeholder="Номер телефона" mask="+7 (999) 999-99-99" />
        </People>
        <People name="Константин Константинопольский" birthday="28.12.1994">
          <Button color="light-gray" bordered hasRightIcon width="auto">
            <span>Это я</span>
            <Check />
          </Button>
          <Input placeholder="Номер телефона" mask="+7 (999) 999-99-99" />
          <Button color="light-gray" bordered hasRightIcon textAlign="left">
            <span>Мой подопечный</span>
            <Check />
          </Button>
        </People>
      </People.Group>
      <Info classList="oval-mb-64">
        <Subtitle>Важная информация</Subtitle>
        <Paragraph isGray>
          Для выплаты несовершеннолетним нужно получить справку о задержке рейса у авиакомпании,
           и обратиться в наш офис
        </Paragraph>
      </Info>
      <Button>Продолжить</Button>
      <Checkbox classList="oval-m-16">Я подтверждаю, подопечный является моим близким родственником</Checkbox>
    </div>
  </div>
);
