import React from 'react';
import Title from '../components/typography/Title';
import Paragraph from '../components/typography/Paragraph';
import Button from '../components/buttons/Button';
import Logo from '../components/Logo';
import Progress from '../components/Progress';
import Card from '../components/buttons/Card';
import Checkbox from '../components/inputs/Checkbox';

export default () => (
  <div className="oval-root">
    <Progress width="100%" />
    <div className="oval-container">
      <Logo />
      <Title classList="oval-mb-32">Выберите вариант компенсации</Title>
      <Card title="Получить выплату" description="Онлайн на на карту" bordered />
      <Card title="Размещение в VIP-зале" description="Проведите время с комфортом" isOpen>
        <Paragraph isGray>Пожалуйста, заполните форму для получения страховой выплаты</Paragraph>
        <Button selected>+7 495 500-44-32</Button>
        <Checkbox>
          Я подтверждаю получение страхового возмещения в виде сервисных услуг и согласен
          <span className="oval-primary"> с условиями предоставления сервисов</span>
        </Checkbox>
      </Card>
    </div>
  </div>
);
