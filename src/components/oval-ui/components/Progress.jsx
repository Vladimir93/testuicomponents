import React from 'react';
import PropTypes from 'prop-types';

export default function Subtitle({ width }) {
  return (
    <div className="oval-progress" style={{ width }} />
  );
}

Subtitle.defaultProps = {
  width: '0%',
};

Subtitle.propTypes = {
  width: PropTypes.string,
};
