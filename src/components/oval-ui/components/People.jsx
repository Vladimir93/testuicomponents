import React from 'react';
import PropTypes from 'prop-types';

export default function People({ children, name, birthday }) {
  return (
    <div className="oval-peoples__item">
      <span className="oval-peoples__item-title">{name}</span>
      <span className="oval-peoples__item-subtitle">{birthday}</span>
      <div className="oval-peoples__item-row">{children}</div>
    </div>
  );
}

People.defaultProps = {
  children: undefined,
  name: '',
  birthday: '',
};

People.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string,
  birthday: PropTypes.string,
};

People.Group = ({ children }) => <div className="oval-peoples">{children}</div>;

People.Group.defaultProps = {
  children: undefined,
};

People.Group.propTypes = {
  children: PropTypes.node,
};
