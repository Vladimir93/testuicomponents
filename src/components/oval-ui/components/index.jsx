import * as buttons from './buttons';
import * as typography from './typography';
import * as inputs from './inputs';
import Progress from './Progress';
import Preloader from './Preloader';
import Logo from './Logo';
import Info from './Info';
import People from './People';

export {
  buttons,
  typography,
  inputs,
  Progress,
  Preloader,
  Logo,
  Info,
  People,
};
