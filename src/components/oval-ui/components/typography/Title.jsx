import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Title({
  children,
  isLarge,
  textAlign,
  classList,
}) {
  const className = classNames(
    { 'oval-heading-1-large': isLarge },
    { 'oval-heading-1': !isLarge },
    classList,
  );

  return (
    <h1 className={className} style={{ textAlign }}>{children}</h1>
  );
}

Title.defaultProps = {
  children: undefined,
  isLarge: false,
  textAlign: 'left',
  classList: '',
};

Title.propTypes = {
  children: PropTypes.node,
  isLarge: PropTypes.bool,
  textAlign: PropTypes.oneOf(['center', 'left', 'right']),
  classList: PropTypes.string,
};
