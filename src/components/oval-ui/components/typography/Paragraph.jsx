import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Paragraph({
  children,
  textAlign,
  isBold,
  isGray,
  classList,
}) {
  const className = classNames(
    'oval-p',
    { 'oval-bold': isBold },
    { 'oval-gray': isGray },
    classList,
  );

  return (
    <p className={className} style={{ textAlign }}>{children}</p>
  );
}

Paragraph.defaultProps = {
  children: undefined,
  textAlign: 'left',
  isBold: false,
  isGray: false,
  classList: '',
};

Paragraph.propTypes = {
  children: PropTypes.node,
  textAlign: PropTypes.oneOf(['center', 'left', 'right']),
  isBold: PropTypes.bool,
  isGray: PropTypes.bool,
  classList: PropTypes.string,
};
