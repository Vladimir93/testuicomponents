import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Paragraph({
  children,
  textAlign,
  classList,
}) {
  const className = classNames(
    'oval-caption',
    classList,
  );
  return (
    <p className={className} style={{ textAlign }}>{children}</p>
  );
}

Paragraph.defaultProps = {
  children: undefined,
  textAlign: 'left',
  classList: '',
};

Paragraph.propTypes = {
  children: PropTypes.node,
  textAlign: PropTypes.oneOf(['center', 'left', 'right']),
  classList: PropTypes.string,
};
