import Title from './Title';
import Subtitle from './Subtitle';
import Paragraph from './Paragraph';
import Caption from './Caption';

export {
  Title,
  Subtitle,
  Paragraph,
  Caption,
};
