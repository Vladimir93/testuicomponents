import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Subtitle({
  children,
  textAlign,
  classList,
}) {
  const className = classNames(
    'oval-heading-2',
    classList,
  );

  return (
    <h2 className={className} style={{ textAlign }}>{children}</h2>
  );
}

Subtitle.defaultProps = {
  children: undefined,
  textAlign: 'left',
  classList: '',
};

Subtitle.propTypes = {
  children: PropTypes.node,
  textAlign: PropTypes.oneOf(['center', 'left', 'right']),
  classList: PropTypes.string,
};
