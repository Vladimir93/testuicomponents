import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Subtitle({
  children,
  isBlack,
  classList,
}) {
  const className = classNames(
    'oval-info',
    { 'oval-info--black': isBlack },
    classList,
  );
  return (
    <div className={className}>{children}</div>
  );
}

Subtitle.defaultProps = {
  children: undefined,
  isBlack: false,
  classList: '',
};

Subtitle.propTypes = {
  children: PropTypes.node,
  isBlack: PropTypes.string,
  classList: PropTypes.string,
};
