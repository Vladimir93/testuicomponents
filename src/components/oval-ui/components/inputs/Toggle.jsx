import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Toggle({
  children,
  name,
  checked,
  onChange,
  disabled,
  classList,
}) {
  const className = classNames(
    'oval-toggle',
    { 'oval-toggle--disabled': disabled },
    classList,
  );

  return (
    <label className={className}>
      <input name={name} type="checkbox" className="oval-toggle__native" checked={checked} onChange={onChange} />
      <span className="oval-toggle__box" />
      <p className="oval-toggle__label oval-p">{children}</p>
    </label>
  );
}

Toggle.defaultProps = {
  children: undefined,
  name: '',
  checked: false,
  onChange: () => {},
  disabled: false,
  classList: '',
};

Toggle.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  classList: PropTypes.string,
};
