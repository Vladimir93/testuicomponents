import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Radio({
  children,
  name,
  checked,
  onChange,
  disabled,
  classList,
}) {
  const className = classNames(
    'oval-radio',
    { 'oval-radio--disabled': disabled },
    classList,
  );

  return (
    <label className={className}>
      <input name={name} type="radio" className="oval-radio__native" checked={checked} onChange={onChange} />
      <span className="oval-radio__box" />
      <p className="oval-radio__label oval-p">{children}</p>
    </label>
  );
}

Radio.defaultProps = {
  children: undefined,
  name: '',
  checked: false,
  onChange: () => {},
  disabled: false,
  classList: '',
};

Radio.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  classList: PropTypes.string,
};

Radio.Group = ({ children }) => <div className="oval-radio-group">{children}</div>;

Radio.Group.defaultProps = {
  children: undefined,
};

Radio.Group.propTypes = {
  children: PropTypes.node,
};
