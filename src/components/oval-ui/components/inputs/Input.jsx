import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import InputMask from 'react-input-mask';

export default function Input({
  name,
  type,
  placeholder,
  value,
  onChange,
  onInput,
  disabled,
  error,
  filled,
  width,
  mask,
  classList,
}) {
  const className = classNames(
    'oval-input',
    { 'oval-input--has-value': value.length > 0 },
    { 'oval-input--filled': filled },
    { 'oval-input--error': error.length > 0 },
    { 'oval-input--disabled': disabled },
    { 'oval-input--file': type === 'file' },
    classList,
  );

  const input = (
    <label className={className} style={{ width }}>
      <InputMask mask={mask} name={name} type={type} className="oval-input__native" onChange={onChange} onInput={onInput} value={value} disabled={disabled} />
      <span className="oval-input__placeholder">{error.length > 0 ? error : placeholder}</span>
    </label>
  );

  const fileInput = (
    <label className={className} style={{ width }}>
      <input name={name} type={type} style={{ display: 'none' }} onChange={onChange} onInput={onInput} value={value} disabled={disabled} />
      <span className="oval-input__native" />
      <span className="oval-input__placeholder">{error.length > 0 ? error : placeholder}</span>
    </label>
  );

  return type === 'file' ? fileInput : input;
}

Input.defaultProps = {
  name: '',
  type: 'text',
  placeholder: '',
  value: '',
  onChange: () => {},
  onInput: () => {},
  disabled: false,
  error: '',
  filled: false,
  width: '100%',
  mask: undefined,
  classList: '',
};

Input.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onInput: PropTypes.func,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  filled: PropTypes.bool,
  width: PropTypes.string,
  mask: PropTypes.string,
  classList: PropTypes.string,
};

Input.Group = ({ children }) => <div className="oval-input-list">{children}</div>;

Input.Group.defaultProps = {
  children: undefined,
};

Input.Group.propTypes = {
  children: PropTypes.node,
};
