import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Checkbox({
  children,
  name,
  checked,
  onChange,
  disabled,
  classList,
}) {
  const className = classNames(
    'oval-checkbox',
    { 'oval-checkbox--disabled': disabled },
    classList,
  );

  return (
    <label className={className}>
      <input name={name} type="checkbox" className="oval-checkbox__native" checked={checked} onChange={onChange} />
      <span className="oval-checkbox__box" />
      <p className="oval-checkbox__label oval-p">{children}</p>
    </label>
  );
}

Checkbox.defaultProps = {
  children: undefined,
  name: '',
  checked: false,
  onChange: () => {},
  disabled: false,
  classList: '',
};

Checkbox.propTypes = {
  children: PropTypes.node,
  name: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  classList: PropTypes.string,
};
