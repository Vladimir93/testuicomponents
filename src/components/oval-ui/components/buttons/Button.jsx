import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Button({
  children,
  onClick,
  onMouseEnter,
  onMouseLeave,
  size,
  disabled,
  color,
  bordered,
  selected,
  hasError,
  width,
  hasLeftIcon,
  hasRightIcon,
  textAlign,
  classList,
}) {
  const className = classNames(
    'oval-button',
    `oval-button--size_${size}`,
    `oval-button--color_${color}`,
    { 'oval-button--disabled': disabled },
    { 'oval-button--bordered': bordered },
    { 'oval-button--selected': selected },
    { 'oval-button--has-error': hasError },
    { 'oval-button--icon_both': hasLeftIcon && hasRightIcon },
    { 'oval-button--icon_left': hasLeftIcon && !hasRightIcon },
    { 'oval-button--icon_right': !hasLeftIcon && hasRightIcon },
    classList,
  );

  return (
    <button
      type="button"
      className={className}
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      disabled={disabled}
      style={{ width, textAlign }}
    >
      {children}
    </button>
  );
}

Button.defaultProps = {
  children: undefined,
  onClick: () => {},
  onMouseEnter: () => {},
  onMouseLeave: () => {},
  size: 'xxl',
  disabled: false,
  color: 'green',
  bordered: false,
  selected: false,
  hasError: false,
  width: '100%',
  hasLeftIcon: false,
  hasRightIcon: false,
  textAlign: 'center',
  classList: '',
};

Button.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  size: PropTypes.oneOf(['small', 'large', 'xl', 'xxl']),
  disabled: PropTypes.bool,
  color: PropTypes.oneOf(['black', 'green', 'yellow', 'silver', 'gray', 'light-gray', 'red']),
  bordered: PropTypes.bool,
  selected: PropTypes.bool,
  hasError: PropTypes.bool,
  width: PropTypes.string,
  hasLeftIcon: PropTypes.bool,
  hasRightIcon: PropTypes.bool,
  textAlign: PropTypes.oneOf(['center', 'left', 'right']),
  classList: PropTypes.string,
};
