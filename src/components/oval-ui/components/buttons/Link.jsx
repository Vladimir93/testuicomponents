import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Link({
  children,
  href,
  onClick,
  onMouseEnter,
  onMouseLeave,
  disabled,
  style,
  classList,
}) {
  const className = classNames(
    'oval-link',
    { 'oval-link--disabled': disabled },
    classList,
  );

  const link = (
    <a
      href={href}
      className={className}
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      disabled={disabled}
      style={style}
    >
      {children}
    </a>
  );

  const button = (
    <button
      type="button"
      className={className}
      onClick={onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      disabled={disabled}
    >
      {children}
    </button>
  );

  return href.length > 0 ? link : button;
}

Link.defaultProps = {
  children: undefined,
  href: '',
  onClick: () => {},
  onMouseEnter: () => {},
  onMouseLeave: () => {},
  disabled: false,
  classList: '',
};

Link.propTypes = {
  children: PropTypes.node,
  href: PropTypes.string,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  disabled: PropTypes.bool,
  classList: PropTypes.string,
};
