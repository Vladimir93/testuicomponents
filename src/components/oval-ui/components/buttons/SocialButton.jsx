import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function SocialButton({
  children,
  onClick,
  href,
  background,
}) {
  const className = classNames(
    'oval-social',
    { 'oval-social--gray': !background },
  );

  return (
    <a
      href={href}
      target={href ? '_blank' : false}
      rel={href ? 'noopener noreferrer' : false}
      onClick={onClick}
      className={className}
      style={{ background }}
    >
      {children}
    </a>
  );
}

SocialButton.defaultProps = {
  children: undefined,
  onClick: () => {},
  href: '#',
  background: '',
};

SocialButton.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  href: PropTypes.string,
  background: PropTypes.string,
};

SocialButton.Group = ({ children, title }) => (
  <div className="oval-social-group">
    {title.length > 0 && <span className="oval-social-group-title">{title}</span>}
    <div className="oval-social-list">{children}</div>
  </div>
);

SocialButton.Group.defaultProps = {
  children: undefined,
  title: '',
};

SocialButton.Group.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};
