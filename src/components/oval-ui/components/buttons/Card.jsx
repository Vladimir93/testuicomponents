import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Paragraph from '../typography/Paragraph';

export default function Card({
  children,
  onClick,
  title,
  description,
  bordered,
  isOpen,
  classList,
}) {
  const className = classNames(
    'oval-card',
    { 'oval-card--bordered': bordered },
    { 'oval-card--toggle': children },
    { 'oval-card--is-open': isOpen },
    classList,
  );

  const cardWithToggle = (
    <div className={className}>
      <button type="button" onClick={onClick} className="oval-card__header">
        <span className="oval-card__title">{title}</span>
        {(!children || !isOpen) && <Paragraph isGray>{description}</Paragraph>}
      </button>
      {isOpen && <div className="oval-card__content">{children}</div>}
    </div>
  );

  const cardWithLink = (
    <button type="button" onClick={onClick} className={className}>
      <span className="oval-card__title">{title}</span>
      <Paragraph isGray>{description}</Paragraph>
    </button>
  );

  return children ? cardWithToggle : cardWithLink;
}

Card.defaultProps = {
  children: undefined,
  onClick: () => {},
  title: '',
  description: '',
  bordered: false,
  isOpen: false,
  classList: '',
};

Card.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  title: PropTypes.string,
  description: PropTypes.string,
  bordered: PropTypes.bool,
  isOpen: PropTypes.bool,
  classList: PropTypes.string,
};
