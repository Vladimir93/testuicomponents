import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default function Subtitle({
  visible,
  animation,
  label,
}) {
  const className = classNames(
    'oval-preloader',
    { 'oval-preloader--visiable': visible },
  );

  return (
    <div className={className}>
      <img src={animation} className="oval-preloader__img" alt="" />
      <span className="oval-preloader__label">{label}</span>
    </div>
  );
}

Subtitle.defaultProps = {
  visible: true,
  animation: '',
  label: 'Загружаем страницу...',
};

Subtitle.propTypes = {
  visible: PropTypes.bool,
  animation: PropTypes.string,
  label: PropTypes.string,
};
