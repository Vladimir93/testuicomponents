import React from 'react';
import './App.css';
import './components/oval-ui/stylesheets/main.scss';

/*
import Info from './components/oval-ui/components/Info';
import Logo from './components/oval-ui/components/Logo';
import People from './components/oval-ui/components/People';
import Preloader from './components/oval-ui/components/Preloader';
import Progress from './components/oval-ui/components/Progress';
import Button from './components/oval-ui/components/buttons/Button';
import Card from './components/oval-ui/components/buttons/Card';
import Link from './components/oval-ui/components/buttons/Link';
import SocialButton from './components/oval-ui/components/buttons/SocialButton';
import Checkbox from './components/oval-ui/components/inputs/Checkbox';
import Input from './components/oval-ui/components/inputs/Input';
import Radio from './components/oval-ui/components/inputs/Radio';
import Toggle from './components/oval-ui/components/inputs/Toggle';
import Caption from './components/oval-ui/components/typography/Caption';
import Paragraph from './components/oval-ui/components/typography/Paragraph';
import Subtitle from './components/oval-ui/components/typography/Subtitle';
import Title from './components/oval-ui/components/typography/Title';
*/

import Preloader from './components/oval-ui/components/Preloader';
import Title from './components/oval-ui/components/typography/Title';
import Paragraph from './components/oval-ui/components/typography/Paragraph';
import Input from './components/oval-ui/components/inputs/Input';
import Button from './components/oval-ui/components/buttons/Button';
import Logo from './components/oval-ui/components/Logo';
import Progress from './components/oval-ui/components/Progress';

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      phone: '',
      flightNumber: '',
    }

    this.phoneChange = this.phoneChange.bind(this);
    this.flightNumberChange = this.flightNumberChange.bind(this);
    this.submit = this.submit.bind(this);
  }

  phoneChange(event){
    this.setState({ phone: event.target.value });
  }

  flightNumberChange(event){
    this.setState({ flightNumber: event.target.value });
  }

  submit(event){
    alert(`Номер телефона: ${this.state.phone}\nНомер рейса: ${this.state.flightNumber}`);
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 1000);
  }

  render() {

    return (
      <div className="oval-root">
        <Progress width="100%" />
        <Preloader visible={this.state.isLoading} animation="img/preloader.gif" />
        <div className="oval-container">
          <Logo />
          <Title>Ваш рейс задержан?</Title>
          <Paragraph isGray>Пожалуйста, заполните форму для получения страховой выплаты</Paragraph>
          <div className="oval-mb-32">
            <Input placeholder="Номер телефона" mask="+7 (999) 999-99-99" value={this.state.phone} onChange={this.phoneChange} />
            <Input placeholder="Номер рейса" mask="aa 9999" value={this.state.flightNumber} onChange={this.flightNumberChange} />
          </div>
          <Button onClick={this.submit}>Продолжить</Button>
        </div>
      </div>
    );
  }
}

export default App;
